package com.vijay.shoppingapp.service;

import com.vijay.shoppingapp.db.entities.Customer;
import com.vijay.shoppingapp.db.entities.Order;
import com.vijay.shoppingapp.db.entities.OrderLineItem;
import com.vijay.shoppingapp.db.entities.Product;
import com.vijay.shoppingapp.db.repositories.OrderRepository;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
class OrderServiceTest {

    @MockBean
    private OrderRepository orderRepository;
    @MockBean
    private ProductService productService;
    @MockBean
    private CustomerService customerService;

    @Autowired
    private OrderService orderService;

    @Test
    void test_getAllOrders()  {
        orderService.findAllOrders();
        verify(orderRepository).findAll();
    }

    @Test
    void test_createOrder_with_nullInput_returns_null()  {
        assertNull(orderService.createOrder(null));
    }

    @Test
    void test_createOrder_with_noLineItem_returns_null()  {
        assertNull(orderService.createOrder(new Order()));
    }

    @Test
    void test_createOrder_with_emptyProduct_returns_null()  {
        Order order = new Order();
        order.getLineItems().add(new OrderLineItem());
        assertNull(orderService.createOrder(order));
    }

    @Test
    void test_createOrder_with_noProductId_returns_null()  {
        Order order = new Order();
        order.getLineItems().add(new OrderLineItem(10, order, new Product()));
        assertNull(orderService.createOrder(order));
    }
    @Test
    void test_createOrder_with_nonExistentProductId_returns_null()  {
        Order order = new Order();
        Product product = new Product("prod1", "product 1", 100.5);
        product.setProductId(100L);
        order.getLineItems().add(new OrderLineItem(10, order, product));
        when(productService.findById(100L)).thenReturn(null);
        assertNull(orderService.createOrder(order));
    }

    @Test
    void test_createOrder_with_nullCustomer_returns_null()  {
        Order order = new Order();
        Product product = new Product("prod1", "product 1", 100.5);
        product.setProductId(100L);
        order.getLineItems().add(new OrderLineItem(10, order, product));
        when(productService.findById(100L)).thenReturn(validProduct(100L));
        assertNull(orderService.createOrder(order));
    }

    @Test
    void test_createOrder_with_nullCustomerID_returns_status_badrequest()  {
        Order order = new Order();
        order.setCustomer(new Customer());
        Product product = new Product("prod1", "product 1", 100.5);
        product.setProductId(100L);
        order.getLineItems().add(new OrderLineItem(10, order, product));
        when(productService.findById(100L)).thenReturn(validProduct(100L));
        assertNull(orderService.createOrder(order));
    }

    @Test
    void test_createOrder_with_NonExistentCustomerID_returns_null()  {
        Order order = new Order();
        Customer customer = new Customer();
        customer.setCustomerId(10L);
        order.setCustomer(customer);
        Product product = new Product("prod1", "product 1", 100.5);
        product.setProductId(100L);
        order.getLineItems().add(new OrderLineItem(10, order, product));
        when(productService.findById(100L)).thenReturn(validProduct(100L));
        when(customerService.findById(10L)).thenReturn(null);
        assertNull(orderService.createOrder(order));
    }

    @Test
    void test_createOrder_with_allvalidinput_returns_validOrder()  {
        Order order = new Order();
        Customer customer = new Customer();
        customer.setCustomerId(10L);
        order.setCustomer(customer);
        Product product = new Product();
        product.setProductId(100L);
        order.getLineItems().add(new OrderLineItem(10, order, product));
        when(productService.findById(100L)).thenReturn(validProduct(100L));
        Customer customerFromDb = validCustomer(10L);
        when(customerService.findById(10L)).thenReturn(customerFromDb);

        ArgumentCaptor<Order> orderArgumentCaptor = ArgumentCaptor.forClass(Order.class);
        when(orderRepository.save(orderArgumentCaptor.capture())).thenReturn(order);
        assertNotNull(orderService.createOrder(order));

        Order savedOrder = orderArgumentCaptor.getValue();
        assertEquals(customerFromDb, savedOrder.getCustomer());
        assertEquals(1000.0, savedOrder.getTotalOrderPrice());
    }

    private Customer validCustomer(long customerId) {
        Customer customer = new Customer();
        customer.setCustomerId(customerId);
        customer.setName("customer "+customerId);
        customer.setEmail(customerId+"@customer");
        customer.setCreateDate(LocalDateTime.now());
        return customer;
    }

    private Product validProduct(Long productId) {
        return new Product("prod"+productId, "prod desc "+productId, productId.doubleValue());
    }

    //TODO
    //Write tests for search between date range
}