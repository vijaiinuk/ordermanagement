package com.vijay.shoppingapp.service;

import com.vijay.shoppingapp.db.entities.Product;
import com.vijay.shoppingapp.db.repositories.ProductRepository;
import com.vijay.shoppingapp.exception.InvalidOperationException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class ProductServiceTest {

    @MockBean
    private ProductRepository productRepository;

    @Autowired
    private ProductService productService;

    @Test
    void getAllProducts() {
        productService.getAllProducts();
        verify(productRepository).findAll();
    }

    @Test
    void createProduct() throws InvalidOperationException {
        Product product = new Product();
        product.setName("product1");
        product.setDescription("sample product");
        product.setPrice(10.5);
        productService.createProduct(product);
        verify(productRepository).save(product);
    }

    @Test
    void updateProduct() {
        Product givenProduct = new Product();
        givenProduct.setName("product2");
        givenProduct.setDescription("sample product");
        givenProduct.setPrice(20.5);

        Product returnProduct = new Product();
        returnProduct.setName("product3");
        returnProduct.setDescription("return sample product");
        returnProduct.setPrice(10.0);
        when(productRepository.findById(any())).thenReturn(Optional.of(returnProduct));
        productService.updateProduct(givenProduct, givenProduct.getProductId());

        verify(productRepository).save(any(Product.class));
        assertEquals(givenProduct.getPrice(), returnProduct.getPrice());
        assertEquals(givenProduct.getName(), returnProduct.getName());
        assertEquals(givenProduct.getDescription(), returnProduct.getDescription());
    }
}