package com.vijay.shoppingapp.controller;

import com.vijay.shoppingapp.db.entities.Customer;
import com.vijay.shoppingapp.exception.InvalidOperationException;
import com.vijay.shoppingapp.service.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/customer")
@Api(value = "Customer Controller")
public class CustomerController {

    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @ApiOperation(value = "Create customer, pass customer without customerId. email, name are mandatory fields")
    @PostMapping(value = "/createCustomer", consumes = "application/json")
    public ResponseEntity<Customer> createCustomer(@RequestBody Customer customer)  {
        Customer savedCustomer = null;
        try {
            savedCustomer = customerService.createCustomer(customer);
        } catch (InvalidOperationException e) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(savedCustomer);
    }

    @ApiOperation(value = "Find Customer by Email.  email is unique to a customer")
    @GetMapping("/findByEmail")
    public ResponseEntity<Customer> getCustomerByEmail(String email)  {
        Customer customer = customerService.findByEmail(email);
        if(Objects.nonNull(customer))  {
            return ResponseEntity.ok(customer);
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(value = "Find Customer by Name.")
    @GetMapping("/findByName")
    public ResponseEntity<List<Customer>> getCustomerByName(String name)  {
        List<Customer> customer = customerService.findByName(name);
        return ResponseEntity.ok(customer);
    }

    @ApiOperation(value = "Delete a customer by id")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteCustomer(@RequestParam Long id)  {
        customerService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/createAllCustomer")
    public void createCustomer()  {
        Customer customer = new Customer();
        customer.setEmail("test@gmail");
        customer.setName("test");
        createCustomer(customer);
    }
}
