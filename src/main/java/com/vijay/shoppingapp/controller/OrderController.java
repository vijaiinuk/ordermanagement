package com.vijay.shoppingapp.controller;

import com.vijay.shoppingapp.db.entities.Customer;
import com.vijay.shoppingapp.db.entities.Order;
import com.vijay.shoppingapp.db.entities.OrderLineItem;
import com.vijay.shoppingapp.db.entities.Product;
import com.vijay.shoppingapp.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

@Slf4j
@RestController
@Api(value = "/order")
public class OrderController {

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @ApiOperation(value = "Get all orders")
    @GetMapping("/getAllOrders")
    public ResponseEntity<List<Order>> getAllOrders()  {
        return ResponseEntity.ok(orderService.findAllOrders());
    }

    @ApiOperation(value = "Get orders placed between dates.  Provide date in yyyy-MM-dd HH:mm:ss format")
    @GetMapping("/getOrdersPlacedBetween")
    public ResponseEntity<List<Order>> getAllOrdersBetween(String fromDateTime, String toDateTime)  {
        List<Order> searchResults = orderService.getAllOrdersBetween(fromDateTime, toDateTime);
        return ResponseEntity.ok(searchResults);
    }

    @ApiOperation(value = "Create new order.  Provide each orderLineItem with quantity and existing product id")
    @PostMapping("/createOrder")
    public ResponseEntity<Order> createOrder(@RequestBody Order order)  {
        Order savedOrder = orderService.createOrder(order);
        if(Objects.isNull(savedOrder))
            return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(savedOrder);
    }

    @PostMapping("/createAll")
    public void onLoad()  {
        Order order = new Order();
        Customer customer = new Customer();
        customer.setCustomerId(1L);
        order.setCustomer(customer);
        Product product = new Product();
        product.setProductId(1L);
        OrderLineItem lineItem = new OrderLineItem(10, order, product );
        order.getLineItems().add(lineItem);
        Product product2 = new Product();
        product2.setProductId(2L);
        order.getLineItems().add(new OrderLineItem(10, order, product2 ));
        createOrder(order);
    }

}
