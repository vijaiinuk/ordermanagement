package com.vijay.shoppingapp.controller;

import com.vijay.shoppingapp.db.entities.Product;
import com.vijay.shoppingapp.exception.InvalidOperationException;
import com.vijay.shoppingapp.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/product")
@Api(value = "Product Controller")
public class ProductController {

    private final ProductService productService;
    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @ApiOperation(value = "Get all available Products")
    @GetMapping("/getAllProducts")
    public ResponseEntity<List<Product>> getAllProducts()  {
        return ResponseEntity.ok(productService.getAllProducts());
    }

    @ApiOperation(value = "Create a new product. productId should be null and price should not be negative ")
    @PostMapping(value = "/createProduct")
    public ResponseEntity<Product> createProduct(@RequestBody Product product)  {
        Product createdProduct = null;
        try {
            createdProduct = productService.createProduct(product);
        } catch (InvalidOperationException e) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(createdProduct);
    }

    @ApiOperation(value = "update a product with product id")
    @PutMapping("/update/{id}")
    public ResponseEntity<Product> updateProduct(@RequestBody Product product, @RequestParam Long id)  {
        Product updateProduct = productService.updateProduct(product, id);
        if(Objects.nonNull(updateProduct))  {
            return ResponseEntity.ok(updateProduct);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @ApiOperation(value = "Delete a product with product id")
    @PutMapping("/delete/{id}")
    public ResponseEntity deleteProduct(@RequestParam Long id)  {
        productService.deleteProduct(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/createAllProduct")
    public void createProduct()  {
        Product product = new Product();
        product.setPrice(10.0);
        product.setName("Prod 1");
        createProduct(product);
        Product product1 = new Product();
        product1.setPrice(20.0);
        product1.setName("Prod 2");
        createProduct(product1);
    }

}
