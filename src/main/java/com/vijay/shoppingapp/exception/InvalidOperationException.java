package com.vijay.shoppingapp.exception;

public class InvalidOperationException extends Exception {
    public InvalidOperationException() {
    }

    public InvalidOperationException(String message) {
        super(message);
    }
}
