package com.vijay.shoppingapp.service;

import com.vijay.shoppingapp.db.entities.Product;
import com.vijay.shoppingapp.db.repositories.ProductRepository;
import com.vijay.shoppingapp.exception.InvalidOperationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ProductService {
    private final ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public Product createProduct(Product product) throws InvalidOperationException {
        validateNewProduct(product);
        return productRepository.save(product);
    }

    public Product updateProduct(Product product, Long id)  {
        Optional<Product> foundProduct = productRepository.findById(id);
        foundProduct.ifPresent(product1 -> {
            if(Objects.nonNull(product.getName()))
                product1.setName(product.getName());
            if(Objects.nonNull(product.getDescription()))
                product1.setDescription(product.getDescription());
            if(Objects.nonNull(product.getPrice()))
                product1.setPrice(product.getPrice());
            productRepository.save(product1);
        });
        return foundProduct.orElse(null);

    }

    public void deleteProduct(Long id)  {
        productRepository.deleteById(id);
    }

    private void validateNewProduct(Product product) throws InvalidOperationException {
        if(Objects.isNull(product))
            throw new InvalidOperationException("Product null for new product");
        if(Objects.nonNull(product.getProductId()))
            throw new InvalidOperationException("Product id cannot be set for new product");
        if(Objects.isNull(product.getName()) || Objects.isNull(product.getPrice()) || product.getPrice() < 0)
            throw new InvalidOperationException("Need Product Name and positive price for the new product");
    }

    public Product findById(Long productId) {
        return productRepository.findById(productId).orElse(null);
    }
}