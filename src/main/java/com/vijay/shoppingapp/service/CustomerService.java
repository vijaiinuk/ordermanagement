package com.vijay.shoppingapp.service;

import com.vijay.shoppingapp.db.entities.Customer;
import com.vijay.shoppingapp.db.repositories.CustomerRepository;
import com.vijay.shoppingapp.exception.InvalidOperationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
public class CustomerService {
    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Customer createCustomer(Customer customer) throws InvalidOperationException {
        validateCustomer(customer);
        if(Objects.isNull(customer.getCreateDate()))
            customer.setCreateDate(LocalDateTime.now());
        return customerRepository.save(customer);

    }

    private void validateCustomer(Customer customer) throws InvalidOperationException {
        if(Objects.isNull(customer))
            throw new InvalidOperationException("Customer cannot be null");
        if(Objects.nonNull(customer.getCustomerId()))
            throw new InvalidOperationException("Customer Id cannot be set for new Customer");
        if(Objects.isNull(customer.getEmail()) || Objects.isNull(customer.getName()))
            throw new InvalidOperationException("Customer Name and Email cannot be empty");
    }

    public Customer findByEmail(String email) {
        return customerRepository.findCustomerByEmail(email);
    }
    public List<Customer> findByName(String name) {
        return customerRepository.findAllByName(name);
    }

    public void deleteById(Long id) {
        customerRepository.deleteById(id);
    }

    public Customer findById(Long customerId) {
        return customerRepository.findById(customerId).orElse(null);
    }
}
