package com.vijay.shoppingapp.service;

import com.vijay.shoppingapp.db.entities.Customer;
import com.vijay.shoppingapp.db.entities.Order;
import com.vijay.shoppingapp.db.entities.OrderLineItem;
import com.vijay.shoppingapp.db.entities.Product;
import com.vijay.shoppingapp.db.repositories.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
@Service
public class OrderService {

    private final OrderRepository orderRepository;
    private final ProductService productService;
    private final CustomerService customerService;

    private final String ORDER_RECEIVED = "RECEIVED";

    public OrderService(OrderRepository orderRepository, ProductService productService, CustomerService customerService) {
        this.orderRepository = orderRepository;
        this.productService = productService;
        this.customerService = customerService;
    }

    public List<Order> findAllOrders() {
        return orderRepository.findAll();
    }

    public List<Order> getAllOrdersBetween(String fromDateTime, String toDateTime)  {
        LocalDateTime fromTime = convertToLocalDateTime(fromDateTime);
        LocalDateTime endTime = convertToLocalDateTime(toDateTime);
        List<Order> searchResults = orderRepository.findAllByCreateDateBetween(fromTime, endTime);
        return searchResults;
    }

    public Order createOrder(@RequestBody Order order)  {
        if(Objects.isNull(order) || validLineItem(order)  || validCustomer(order.getCustomer()))
            return null;

        AtomicBoolean validLineItems = new AtomicBoolean(true);

        order.getLineItems().forEach(orderLineItem -> {
            Product product = productService.findById(orderLineItem.getProduct().getProductId());
            if(Objects.nonNull(product))  {
                orderLineItem.setProduct(product);
                orderLineItem.setTotalPrice(product.getPrice() * orderLineItem.getQuantity());
                orderLineItem.setOrder(order);
            } else {
                validLineItems.set(false);
                log.warn("Ignoring Order line Item (due to invalid Product Id) : {} ", orderLineItem);
            }
        });
        if(!validLineItems.get())  {
            log.warn("Invalid LineItem or Product");
            return null;
        }
        Customer customer = customerService.findById(order.getCustomer().getCustomerId());
        if(Objects.isNull(customer))  {
            log.warn("Invalid customer");
            return null;
        }
        order.setCustomer(customer);
        order.setStatus(ORDER_RECEIVED);
        order.setCreateDate(LocalDateTime.now());
        order.postLoad();
        Order savedOrder = orderRepository.save(order);
        log.info("Order persisted: {}", savedOrder);
        return savedOrder;
    }

    private boolean validCustomer(Customer customer) {
        return Objects.isNull(customer) || Objects.isNull(customer.getCustomerId());
    }

    private boolean validLineItem(Order order) {
        return order.getLineItems().isEmpty()
                || order.getLineItems().stream().map(OrderLineItem::getProduct).anyMatch(Objects::isNull)
                || order.getLineItems().stream().map(item -> item.getProduct().getProductId()).anyMatch(Objects::isNull);
    }

    private LocalDateTime convertToLocalDateTime(String fromDateTime) {
        Instant fromInstant = Instant.parse(fromDateTime);
        return LocalDateTime.ofInstant(fromInstant, ZoneId.of(ZoneOffset.UTC.getId()));
    }

}
