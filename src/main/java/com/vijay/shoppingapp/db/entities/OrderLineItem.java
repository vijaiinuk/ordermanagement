package com.vijay.shoppingapp.db.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@ToString
@Data
@Entity
public class OrderLineItem implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "order_id", referencedColumnName = "o_order_id", insertable = false, updatable = false, nullable = false)
    @JsonBackReference
    @ToString.Exclude
    private Order order;

    @Id
    @ManyToOne
    @JoinColumn(name = "product_id", insertable = false, updatable = false, nullable = false)
    private Product product;

    @Basic
    private Integer quantity;

    @Basic
    private Double totalPrice;

    public OrderLineItem() {
    }

    public OrderLineItem(Integer quantity, Order order, Product product) {
        this.quantity = quantity;
        this.order = order;
        this.product = product;
    }
}
