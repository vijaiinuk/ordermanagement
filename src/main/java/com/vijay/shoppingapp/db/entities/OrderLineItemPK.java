//package com.vijay.shoppingapp.db.entities;
//
//import javax.persistence.Embeddable;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import java.io.Serializable;
//import java.util.Objects;
//
////@Embeddable
//public class OrderLineItemPK implements Serializable {
//
//    @ManyToOne
//    @JoinColumn(name = "order_id", updatable = false, insertable = false, nullable = false)
//    private Order order;
//
//    @ManyToOne
//    @JoinColumn(name = "product_id", updatable = false, insertable = false, nullable = false)
//    private Product product;
//
//    public OrderLineItemPK() {
//    }
//
//    public OrderLineItemPK(Order order, Product product) {
//        this.order = order;
//        this.product = product;
//    }
//
//    public Order getOrder() {
//        return order;
//    }
//
//    public void setOrder(Order order) {
//        this.order = order;
//    }
//
//    public Product getProduct() {
//        return product;
//    }
//
//    public void setProduct(Product product) {
//        this.product = product;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        OrderLineItemPK that = (OrderLineItemPK) o;
//        return Objects.equals(order, that.order) &&
//                Objects.equals(product, that.product);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(order, product);
//    }
//}
