package com.vijay.shoppingapp.db.repositories;

import com.vijay.shoppingapp.db.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository  extends JpaRepository<Product, Long> {

}
