package com.vijay.shoppingapp.db.repositories;

import com.vijay.shoppingapp.db.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findAllByCreateDateBetween(LocalDateTime startDate, LocalDateTime endDate);
}
