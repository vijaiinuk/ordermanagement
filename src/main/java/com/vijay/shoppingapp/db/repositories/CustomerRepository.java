package com.vijay.shoppingapp.db.repositories;

import com.vijay.shoppingapp.db.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Customer findCustomerByEmail(String email);
    List<Customer> findAllByName(String name);
}
