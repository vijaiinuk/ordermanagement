Order Management Spring Boot Application
-----------------------------------------

This is a Spring Boot application for Creating/Reading/Updating/Deleting Orders, Products and Customer.

**Technical Stack:**

Java 11

Spring Boot

H2 Database

Swagger/UI

JUnit, Mockito

###### How to run the application?
1. Perform a maven build, by executing the below command:
`mvn clean install`
2. Run the generated jar file from target folder (please make sure you use JRE/JDK 11):
`java -jar target\shoppingapp-0.0.1-SNAPSHOT.jar`
3. Launch the Swagger Documentation from the url- http://localhost:7005/swagger-ui.html#
4. Swagger UI is self explanatory.  Please make sure that you create Product and Customer before 
attempting to create Orders, as Order needs the Product Id and Customer Id.
5. You can use this url to check the db:  http://localhost:7005/h2-console/

 

Also you can use the below curl commands to create the order entry:

`curl --header "Content-Type: application/json"   --request POST   --data '{"name": "John", "email": "john.doe@sample.com" }'   http://localhost:7005/customer/createCustomer
`
`curl --header "Content-Type: application/json"   --request POST   --data '{ "name": "Product 1", "description": "Sample product with price 20", "price": 20.0 }'   http://localhost:7005/product/createProduct`

`curl --header "Content-Type: application/json"   --request POST   --data '{ "lineItems": [ { "product": { "productId": 1 }, "quantity": 10 }, customerId: 1 ] }'   http://localhost:7005/order/createOrder`


Other Considerations:
1. I wanted not to expose the JPA entity on the endpoints as it can lead to attack, by transforming the entity object into a POJO.
   But could not do it, due to time constraint.
2. This application does not authenticate the user.  We can use **Spring JSON Web Token** for authenticating and authorizing users' REST request.
3. For making the service redundant/failover, we should use load balancer to split the REST requests to multiple
 identical microservice instances.
Also on the database, we could use NoSql/document database which can grow horizontally and can be distributed.
